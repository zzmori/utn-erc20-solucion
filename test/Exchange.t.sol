pragma solidity ^0.8.0;

import "forge-std/Test.sol";
import "src/MyERC20.sol";
import "src/Exchange.sol";

contract ExchangeTest is Test {
    MyERC20 public gold;
    MyERC20 public experience;
    Exchange public exchange;
    address public constant USER = address(0xfafa);

    function setUp() public {
        gold = new MyERC20("gold", "GOLD");
        experience = new MyERC20("experience", "XP");
        exchange = new Exchange(address(gold), address(experience));
    }

    function testFailExchangeWithoutGold() public {
        vm.prank(USER);
        exchange.exchange(100);
    }

    function testExchangeRate() public {
        gold.mint(user, 100);
        uint256 balanceBeforeExp = experience.balanceOf(USER);

        vm.prank(USER);
        exchange.exchange(100);

        require(
            balanceBeforeExp < experience.balanceOf(USER),
            "exp balance did not increase"
        );
    }
}