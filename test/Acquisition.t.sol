pragma solidity ^0.8.0;

import "forge-std/Test.sol";
import "src/MyERC20.sol";
import "src/Acquisition.sol";

contract AcquisitionTest is Test {
    MyERC20 public gold;
    MyERC20 public experience;
    Acquisition public acquisition;
    address public constant USER = address(0xfafa);

    function setUp() public {
        gold = new MyERC20("gold", "GOLD");
        experience = new MyERC20("experience", "XP");
        acquisition = new Acquisition(address(gold), address(experience));
    }

    function testAcquisition() public {
        uint256 balanceBeforeGold = gold.balanceOf(USER);
        uint256 balanceBeforeExp = experience.balanceOf(USER);

        vm.prank(USER);
        acquisition.acquire();

        require(
            balanceBeforeGold < gold.balanceOf(USER),
            "gold balance did not go up"
        );

        require(
            balanceBeforeExp < experience.balanceOf(USER),
            "experience balance did not go up"
        );
    }
}